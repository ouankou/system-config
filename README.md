# System Configurations

My bash and vim configuration.

# Guide
Put the SSH key in `.ssh` folder.
Append `bashrc` to the end of `.bashrc`. 

```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

## Git Configuration

Import a key:

```bash
gpg2 --import FILENAME
```

List all secret keys:

```bash
gpg2 --list-secret-keys --keyid-format LONG
```

Edit key setting:

```bash
gpg2 --edit-key KEYCODE
```

#### Global

```bash
git config --global user.name ""
git config --global user.email ""
git config --global user.signingkey
git config --global commit.gpgsign true
git config --global gpg.program gpg2

```

#### Local

```bash
git config user.name "ouankou"
git config user.email "contact@ouankou.com"
git config user.signingkey
```

## Snap

#### Cleanup
`sudo snap list --all | while read snapname ver rev trk pub notes; do if [[ $notes = *disabled* ]]; then sudo snap remove "$snapname" --revision="$rev"; fi; done`

